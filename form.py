# -*- coding: utf-8 -*-
import subprocess
import datetime
from dateutil.tz import gettz
from flask import Flask
from flask import render_template, send_from_directory
from flask.ext.wtf import Form
from flask.ext.bootstrap import Bootstrap
from wtforms import StringField, SubmitField, FileField, RadioField, SelectField
from wtforms.fields.html5 import DateField, EmailField
from wtforms.validators import DataRequired, Email

app = Flask(__name__)
app.config["DEBUG"] = True
app.config["SECRET_KEY"] = "gooddafasfas"
app.config["TEX_DIR"] = "tex"
app.config["PDF_DIR"] = "pdf"
bootstrap = Bootstrap(app)


class NameForm(Form):
    name = StringField(u"姓名", [DataRequired()])
    birth = DateField(u"出生日期", [DataRequired()])
    gender = SelectField(u"性别", choices=[(u"男",u"男"), (u"女",u"女"), (u"其他",u"其他")])
    # select = SelectField("Programming Language", choices=[("cpp","C++"), ("java","Java")])
    # file = FileField("Choose file to upload")
    mail = EmailField(u"邮箱", [Email(), DataRequired()])
    submit = SubmitField(u"提交")


@app.route('/', methods=["GET", "POST"])
def index():
    form = NameForm()
    if form.validate_on_submit():
        name = form.name.data
        birth = form.birth.data
        gender = form.gender.data
        mail = form.mail.data
        tick = datetime.datetime.now(tz=gettz("UTC8"))
        time = str(tick.year)+str(tick.month)+str(tick.day)+str(tick.hour)+str(tick.minute)+str(tick.second)+str(tick.microsecond)[:2]
        output_name = time
        tex_filedir = app.config["TEX_DIR"] + "/" + output_name + ".tex"
        f = open(tex_filedir, mode="w")
        temp = render_template("base.tex", name=name, birth=birth, gender=gender, mail=mail).encode("utf-8")
        f.write(temp)
        f.close()
        subprocess.call(["xelatex", "-output-directory", app.config["PDF_DIR"], tex_filedir])
        pdf_filename = output_name + ".pdf"
        return send_from_directory(app.config["PDF_DIR"], pdf_filename, as_attachment=True)
    return render_template("index.html", form=form)


if __name__ == '__main__':
    app.run("0.0.0.0")
